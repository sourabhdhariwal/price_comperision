-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 02, 2017 at 04:59 PM
-- Server version: 5.7.19
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `price_comparison`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand_master`
--

CREATE TABLE `brand_master` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laptop_other_details`
--

CREATE TABLE `laptop_other_details` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `specification` varchar(50) NOT NULL,
  `specification_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laptop_specification`
--

CREATE TABLE `laptop_specification` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `brands` varchar(50) NOT NULL,
  `usages` varchar(50) NOT NULL,
  `operating_system` varchar(50) NOT NULL,
  `processor_brand` varchar(50) NOT NULL,
  `processor_type` varchar(50) NOT NULL,
  `screen_size` varchar(50) NOT NULL,
  `resolution` varchar(50) NOT NULL,
  `memory` varchar(50) NOT NULL,
  `storage_type` varchar(50) NOT NULL,
  `storage` varchar(50) NOT NULL,
  `graphics_card` varchar(50) NOT NULL,
  `battery_life` varchar(50) NOT NULL,
  `touchscreen` enum('YES','NO') NOT NULL,
  `weight` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_other_details`
--

CREATE TABLE `mobile_other_details` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `specification` varchar(50) NOT NULL,
  `specification_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_other_details`
--

INSERT INTO `mobile_other_details` (`id`, `category_id`, `product_id`, `specification`, `specification_description`) VALUES
(1, 1, 1, 'Primary', '8.M.pixels'),
(2, 1, 1, 'front', '1.2M.pixels'),
(3, 1, 2, 'Primary', '9.M.pixels'),
(4, 1, 2, 'front', '1.5M.pixels');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_specification`
--

CREATE TABLE `mobile_specification` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `brands` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `network` varchar(50) NOT NULL,
  `operating_system` varchar(50) NOT NULL,
  `processor_brand` varchar(50) NOT NULL,
  `processor_type` varchar(50) NOT NULL,
  `screen_size` varchar(50) NOT NULL,
  `resolution` varchar(50) NOT NULL,
  `sim_type` varchar(50) NOT NULL,
  `sim_slot` varchar(50) NOT NULL,
  `display_type` varchar(50) NOT NULL,
  `memory` varchar(50) NOT NULL,
  `internal_storage` varchar(50) NOT NULL,
  `expandable_storage` varchar(50) NOT NULL,
  `primary_camera` varchar(50) NOT NULL,
  `front_camera` varchar(50) NOT NULL,
  `gpu` varchar(50) NOT NULL,
  `battery` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE `order_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `order_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE `product_master` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_condition` enum('fresh','refurbished','','') NOT NULL,
  `product_description` varchar(500) NOT NULL,
  `product_price_ebay` float NOT NULL,
  `product_price_flipkart` float NOT NULL,
  `product_price_amazon` int(11) NOT NULL,
  `product_price_snapdeal` int(11) NOT NULL,
  `product_price_shopmonk` int(11) NOT NULL,
  `product_price_gadget_360` int(11) NOT NULL,
  `product_price_croma` int(11) NOT NULL,
  `product_pricce_tata_cliq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_master`
--

INSERT INTO `product_master` (`id`, `category_id`, `brand_id`, `product_name`, `product_condition`, `product_description`, `product_price_ebay`, `product_price_flipkart`, `product_price_amazon`, `product_price_snapdeal`, `product_price_shopmonk`, `product_price_gadget_360`, `product_price_croma`, `product_pricce_tata_cliq`) VALUES
(1, 1, 1, 'I-Phone5', 'fresh', '', 15, 16, 0, 0, 0, 0, 0, 0),
(2, 1, 1, 'I-Phone6', 'fresh', '', 17, 17, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tablet_other_details`
--

CREATE TABLE `tablet_other_details` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `specification` varchar(50) NOT NULL,
  `specification_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tablet_specification`
--

CREATE TABLE `tablet_specification` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `brands` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `netwrork` varchar(50) NOT NULL,
  `operating_system` varchar(50) NOT NULL,
  `processor_brand` varchar(50) NOT NULL,
  `processor_type` varchar(50) NOT NULL,
  `screen_size` varchar(50) NOT NULL,
  `resolution` varchar(50) NOT NULL,
  `sim_type` varchar(50) NOT NULL,
  `display_type` varchar(50) NOT NULL,
  `memory` varchar(50) NOT NULL,
  `internal_storage` varchar(50) NOT NULL,
  `expandable_storage` varchar(50) NOT NULL,
  `primary_camera` varchar(50) NOT NULL,
  `front_camera` varchar(50) NOT NULL,
  `gpu` varchar(50) NOT NULL,
  `battery` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_details`
--

CREATE TABLE `temp_details` (
  `id` int(11) NOT NULL,
  `signup_token` varchar(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userPassword` varchar(100) NOT NULL,
  `userPhoneno` varchar(100) DEFAULT '0',
  `OTP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_details`
--

INSERT INTO `temp_details` (`id`, `signup_token`, `userName`, `userEmail`, `userPassword`, `userPhoneno`, `OTP`) VALUES
(2, '1509106515836-6KtY31RK-lJWW-g4Oc-etGG-', 'Aditi', 'aditi.mobiwebtech@gmail.com', '415b4c63247f0ab6924bc198f7be156d', '8962469993', 401676),
(3, '1509446709454-x80q7I9L-vSpm-yOrU-V0mU-', 'Preeti', 'Preeti11@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '9074939905', 198888),
(4, '1509446815878-56OFHA1L-gCwP-K8BC-t63N-', 'Nikita', 'Nikita@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '8962469993', 286857),
(5, '1509515330121-VizoDlxa-81vO-gPHM-xk98-', 'Nikita1', 'Nikita1@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '2962353533', 401020),
(6, '1509515347986-yJLk6s1V-chTt-6LGD-twFi-', 'Nikita11', 'Nikita11@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '2962153533', 742744),
(7, '1509515358053-JoHAQsxw-jnTf-pVI2-TkiT-', 'Nikita121', 'Nikita121@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '2062153533', 100184),
(8, '1509517504073-WJStv1y4-eLAc-Y2X8-3mAk-', 'Nikita1321', 'Nikita1321@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '2061153533', 657680),
(9, '1509528741082-dvLuQlfz-yw3Y-Wq6a-sl6Y-', 'Nikita1321', 'Nikita14321@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '2061153533', 223358),
(10, '1509529218428-VBHMleaP-V4ua-MHCV-Rx6b-', 'Aditi Singh', 'aditi.mobiwebtech@gmail.com', '415b4c63247f0ab6924bc198f7be156d', '8962469993', 389550);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` int(11) NOT NULL,
  `transaction_type` enum('Credit','Debit') NOT NULL,
  `cause` enum('By Sign-up','By Single-purchase-item','By Referral') NOT NULL,
  `amount` float NOT NULL,
  `description` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(11) NOT NULL,
  `userType` enum('ADMIN','USER') NOT NULL DEFAULT 'USER',
  `userName` varchar(50) DEFAULT NULL,
  `userEmail` varchar(50) DEFAULT NULL,
  `userPassword` varchar(255) DEFAULT NULL,
  `userPhoneno` varchar(255) DEFAULT NULL,
  `userImage` varchar(255) DEFAULT NULL,
  `userLoginSessionKey` varchar(255) NOT NULL,
  `signup_type` enum('WEB','FACEBOOK','GOOGLE+','YAHOO','0') DEFAULT '0',
  `social_type` enum('FACEBOOK','GOOGLE+','YAHOO','0') DEFAULT '0',
  `social_id` varchar(100) DEFAULT NULL,
  `isSocialSignup` tinyint(2) DEFAULT NULL,
  `referral_code` varchar(100) DEFAULT NULL,
  `userEmailVerified` tinyint(2) DEFAULT NULL,
  `isUserBlocked` int(11) NOT NULL,
  `session_key` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_on` datetime DEFAULT NULL,
  `userLastLogin` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `userLastIpAddress` varchar(100) NOT NULL,
  `isUserDeactivated` int(11) NOT NULL,
  `userTempCode` varchar(255) NOT NULL,
  `userTempCodeSentTime` datetime NOT NULL,
  `isOtpVerified` tinyint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `userType`, `userName`, `userEmail`, `userPassword`, `userPhoneno`, `userImage`, `userLoginSessionKey`, `signup_type`, `social_type`, `social_id`, `isSocialSignup`, `referral_code`, `userEmailVerified`, `isUserBlocked`, `session_key`, `created_on`, `update_on`, `userLastLogin`, `userLastIpAddress`, `isUserDeactivated`, `userTempCode`, `userTempCodeSentTime`, `isOtpVerified`) VALUES
(1, 'ADMIN', 'Admin', 'admin@pricecomparison.com', '0e7517141fb53f21ee439b355b5a1d0a', NULL, NULL, '', '0', NULL, 'aditi.mobiwebtech@gmail.com', NULL, NULL, NULL, 0, NULL, '2017-10-31 13:02:01', NULL, '2017-10-31 13:02:01', '192.168.1.122', 0, '', '0000-00-00 00:00:00', 0),
(8, 'USER', 'Nikita121', 'nikita.mobiwebtech@gmail.com', 'b48e198fa56728d787f2a43917fc9f8e', '2062153533', NULL, '8754d3aa-1698-da19-737c-ec168c0cd783', '0', '0', NULL, NULL, NULL, 1, 0, NULL, '2017-11-01 16:55:10', NULL, '2017-11-01 16:55:10', '', 0, 'http://192.168.1.122:4000/User/verifyEmail/WS2gNLKfKbb1YP30UlC9', '2017-11-01 11:25:10', 1),
(10, 'USER', 'Aditi Singh', 'aditi.mobiwebtech@gmail.com', '415b4c63247f0ab6924bc198f7be156d', '8962469993', NULL, 'c4838c64-f273-e6b2-5746-5f1d0573fc81', '0', '0', NULL, NULL, NULL, 0, 0, NULL, '2017-11-01 16:45:53', NULL, '2017-11-01 16:45:53', '192.168.1.122', 0, '', '0000-00-00 00:00:00', 0),
(11, 'USER', 'Aditi Singh Parihar', NULL, NULL, NULL, NULL, '', '0', 'FACEBOOK', 'aditisinghparihar276@gmail.com', NULL, NULL, NULL, 0, NULL, '2017-11-01 18:01:28', NULL, '2017-11-01 18:01:28', '', 0, '', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mobile_other_details`
--
ALTER TABLE `mobile_other_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_history`
--
ALTER TABLE `order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablet_other_details`
--
ALTER TABLE `tablet_other_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_details`
--
ALTER TABLE `temp_details`
  ADD PRIMARY KEY (`signup_token`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mobile_other_details`
--
ALTER TABLE `mobile_other_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_history`
--
ALTER TABLE `order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_master`
--
ALTER TABLE `product_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tablet_other_details`
--
ALTER TABLE `tablet_other_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_details`
--
ALTER TABLE `temp_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
