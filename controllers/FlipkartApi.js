'use strict';

/*
 * Purpose : For Admin Basic Api
 * Company : Mobiweb Technology Pvt. Ltd.
 * Package : User
 */

var Flipkart     = exports;
var async    = require('async');
var appRoot  = require('app-root-path');
var constant = require(appRoot + '/config/constant.js');
var custom   = require(appRoot + '/lib/custom.js');
var database = require(appRoot + '/config/database.js');
var model    = require(appRoot + '/lib/model.js');
var affiliate = require('flipkart-affiliate-client');
var forEach = require('async-foreach').forEach;
// var randomString = require('random-string');



    /**
     *For Flipkart Api
     *
    */
var client = affiliate.createClient({
    FkAffId: 'devjanidu',
    FkAffToken: '06a66dc9681b44df8963df459d702119',
    responseType: 'json'
  });

var all_api_data=[];
var mobile_laptop_tab=[];
var all_mobile_product_data;
var all_laptop_product_data;
var all_tablete_product_data;
var mobile=[];
var laptops=[];
var tablate=[];
var all_brands = [];
var all_brands1 = [];  


Flipkart.getAllApis = function() {
    client.getCategoryFeed({
        trackingId: 'devjanidu'
       }, function(err, result){
          if(!err){
                 var body =JSON.parse(result);
                 all_api_data.push(body.apiGroups.affiliate.apiListings.mobiles.availableVariants)
                 all_api_data.push(body.apiGroups.affiliate.apiListings.tablets.availableVariants)
                 all_api_data.push(body.apiGroups.affiliate.apiListings.laptops.availableVariants) 
      
               forEach(all_api_data, function(item, index, arr) {
                 for(var propt in item){
                     if(propt=="v1.1.0"){
                         mobile_laptop_tab.push(item[propt])
                     }
                 }
               });
         
               forEach(mobile_laptop_tab, function(item1, index, arr) {
                 client.getProductsFeed({
                       url: item1.get
                    }, function(err, result){
                       if(!err){
                        var products=JSON.parse(result);
                          switch(index)
                            {
                            case 0:
                                all_mobile_product_data= products.productInfoList;
                                console.log(all_mobile_product_data.length);
                                break;
                            case 1:
                                all_tablete_product_data= products.productInfoList;
                                console.log(all_tablete_product_data.length);
                                break;
                            case 2:
                                all_laptop_product_data= products.productInfoList;
                                console.log(all_laptop_product_data.length);
                                break;   
                            }
                        }else {
                            console.log("************",err);
                        }
                    });
                });
            }else {
               console.log("===============",err);
          }
      });
 }


 /**
 * To  Add All Brands
 * 
 */

  Flipkart.GetAllBrand  = function(req,res) {
     //For Get All Brand  Mobile
      forEach(all_mobile_product_data, function(item1, index, arr) {
          mobile.push({brandname:item1.productBaseInfoV1.productBrand})
      })
    
      // FOR FILTER ALL MOBILE BRAND  
         var obj = {};
            for ( var i=0, len=mobile.length; i < len; i++ )
               obj[mobile[i]['brandname']] = mobile[i];
             
               for ( var key in obj )
                   all_brands.push(obj[key]);
       
       //For Get All Brand Laptops
         forEach(all_laptop_product_data, function(item1, index, arr) {
                  laptops.push({brandname:item1.productBaseInfoV1.productBrand})
         })
  
       // FOR FILTER ALL LAPTOP BRAND 
          var obj1 = {};
           for ( var i=0, len=laptops.length; i < len; i++ )
                obj1[laptops[i]['brandname']] = laptops[i];
    
           for ( var key in obj1 )
                 all_brands.push(obj1[key]);
               
       //For Get All Brand Tablete
          forEach(all_tablete_product_data, function(item1, index, arr) {
                    tablate.push({brandname:item1.productBaseInfoV1.productBrand})
             })
  
      // FOR FILTER ALL LAPTOP BRAND 
           var obj2 = {};
            for ( var i=0, len=tablate.length; i < len; i++ )
                  obj2[tablate[i]['brandname']] = tablate[i];
  
            for ( var key in obj2 )
             all_brands.push(obj2[key]);
      
       // FOR SHORT ALL BRAND DATA 
            var obj3 = {};
            for ( var i=0, len=all_brands.length; i < len; i++ )
            obj3[all_brands[i]['brandname']] = all_brands[i];
        
            for ( var key in obj3 )
            all_brands1.push(obj3[key]);
     
         forEach(all_brands1, function(item, index, arr) {
            var user_query = 'SELECT * FROM ' + constant.brand_table  + ' WHERE brand_name ="'+item.brandname+'"';
            database.getConn(user_query, function (err, results) {
                if(err){
                    res.send(custom.dbErrorResponse());
                    return false;
                }
                else{
                    if(results=="")
                            {
                                let Brand_query = 'INSERT INTO ' + constant.brand_table + ' (brand_name) VALUES ("' + item.brandname + '")';
                                database.getConn(Brand_query, function (err, result1) {
                                    if(err){
                                        res.send({
                                                "code": 200,
                                                "response": {},
                                                "status": 0,
                                                "message": err.sqlMessage
                                        });
                                        }else{ }       
                                    });
                             }
                            else{
                                console.log("Already Existing Brand ")          
                                }
                     }
            });
       });
    res.send({
        "code": 200,
        "response":"",
        "status": 1,
        "message": 'Insert All Brand Flipkart Successfully'
    });


}


/**
 * To Add Flipcart Produt 
 * @param {string} product_name

 */

Flipkart.GetAllLaptopProduct  = function(req,res){
  var   Product_data;
  var product_type;
  var image_table;
  var specification;
  switch(req.body.product_name)
  {
      case "MOBILE":
              Product_data=all_mobile_product_data;
           
              product_type="MOBILE";
              image_table=constant.mobile_image;
              specification=constant.mobile_specifications;
               break;
      case "TABLATE":
              Product_data=all_tablete_product_data;
              product_type="TABLATE";
              image_table=constant.tablate_image;
              specification=constant.tablate_specifications;
              break;
      case "LAPTOP":
              Product_data=all_laptop_product_data;
              product_type="LAPTOP";
              image_table=constant.laptop_image;
              specification=constant.laptop_specifications;
                break;
     }

  forEach(Product_data, function(item, index, arr) {
    //   console.log(item)
        /* Check User Email Id and Password */
        async.waterfall([
            function(callback) {
             var user_query = 'SELECT * FROM ' + constant.product_master + ' WHERE product_id="'+ item.productBaseInfoV1.productId+'"';
               database.getConn(user_query, function (err, results) {
                    if(err){
                        res.send({
                            "code": 200,
                            "response": {},
                            "status": 0,
                            "message": err.sqlMessage
                        });
                    }else{
                        if(results != ''){
                            // console.log("Already Existing") 
                            var update_Query = 'UPDATE  ' + constant.product_master + ' SET product_price_flipkart='+item.productBaseInfoV1.flipkartSellingPrice.amount+'  WHERE product_id="'+ item.productBaseInfoV1.productId+'"';
                              database.getConn(update_Query, function (err, results) {
                                  if(err){
                                      res.send({
                                          "code": 200,
                                          "response": {},
                                          "status": 0,
                                          "message": err.sqlMessage
                                      });
                                  }else{
                                     console.log("update Price SuccessFully")
                                  }
                              });
                        }else{
                            callback(null, results);
                         }
                    }
                });
            },
          function(results, callback) {
              var user_query = 'SELECT * FROM ' + constant.brand_table + ' WHERE brand_name="'+ item.productBaseInfoV1.productBrand+'"';
              database.getConn(user_query, function (err, results) {
                  if(err){
                    res.send({
                          "code": 200,
                          "response": {},
                          "status": 0,
                          "message": err.sqlMessage
                      });
                  }else{
                      
                      if(results != ''){
                          callback(null, results);
                      
                      }else{
                          res.send({
                              "code": 200,
                              "response": {},
                              "status": 0,
                              "message": 'Error In get Brand Id '
                          });
                       }
                  }
              });
          },
          function(results, callback) {
          let productDescription=   item.productBaseInfoV1.productDescription;
                if(productDescription=="")
                {
                    productDescription=null
                
                }else{
                    var  p1=productDescription.replace(/['"]+/g, '')
                    productDescription = p1.toString().replace(/"/g, '')
                }
                 /* To get master user id */
                 var product_details="INSERT INTO  " + constant.product_master+ "(`brand_id`, `product_name`,`product_description`,`product_price_flipkart`,`product_type`,`product_id`,`store_url`) VALUES ("+results[0].id+",'"+item.productBaseInfoV1.title+"','"+productDescription+"',"+item.productBaseInfoV1.flipkartSellingPrice.amount +",'"+product_type+"','"+item.productBaseInfoV1.productId+"','"+item.productBaseInfoV1.productUrl+"')";
                  database.getConn(product_details, function (err, detail_results) {
                  if(err){
                      res.send({
                          "code": 200,
                          "response": {},
                          "status": 0,
                          "message": err.sqlMessage
                      });
                  }else{
                      if(detail_results != ''){
                          callback(null, results,detail_results);
                        }else{
                            res.send({
                                "code": 200,
                                "response": {},
                                "status": 0,
                                "message": constant.general_error
                            });
                        }
                  }
              });
          },
          function(results,detail_results,callback) {
                  var p=item.productBaseInfoV1.imageUrls
                  for(var key in p) {

                  if (p.hasOwnProperty(key)) {
                      var image_details="INSERT INTO  " + image_table+ "( `product_id`,`image_size`, `image_url`, `status`) VALUES ("+detail_results.insertId+",'"+key+"','"+p[key]+"','1')";
                      database.getConn(image_details, function (err, detail_results) {
                        if(err){
                            res.send({
                                "code": 200,
                                "response": {},
                                "status": 0,
                                "message": err.sqlMessage
                            });
                        }else{
                            if(detail_results != ''){
                                }else{
                                res.send({
                                    "code": 200,
                                    "response": {},
                                    "status": 0,
                                    "message": constant.general_error
                                });
                            }
                        }
                      });
                  }
              }
              callback(null,detail_results)
           },
           function(detail_results,callback)
           {
             forEach(item.categorySpecificInfoV1.specificationList, function(item2, index2, arr) {
                forEach(item2.values, function(item3, index3, arr) {
                    var  p1=item3.value;
                    var  value = p1.toString().replace(/['"]+/g, '');
                    var P_specification='INSERT INTO ' +specification+ '(`product_id`, `specification`, `specification_description`) VALUES ('+detail_results.insertId+',"'+item3.key+'","'+value+'")';
                    database.getConn(P_specification, function (err, detail_results) {
                         if(err){
                            res.send({
                                "code": 200,
                                "response": {},
                                "status": 0,
                                "message": err.sqlMessage
                            });
                        }else{
                         
                        }
                    });
                })
             })
       }]); 
    })
    res.send({
            "code": 200,
            "response":"" ,
            "status": 1,
            "message": "Add All Mobile Data Successfully have successfully logged in"
        });
  
}






//   Flipkart.GetAllLaptopProduct  = function(req,res){
 
//       forEach(all_tablete_product_data, function(item, index, arr) {
//         console.log(item)
//         /* Check User Email Id and Password */
//         async.waterfall([
//           function(callback) {
//               var user_query = 'SELECT * FROM ' + constant.brand_table + ' WHERE brand_name="'+ item.productBaseInfoV1.productBrand+'"';
            
//               database.getConn(user_query, function (err, results) {
                 
//                   if(err){
                     
//                       res.send({
//                           "code": 200,
//                           "response": {},
//                           "status": 0,
//                           "message": err.sqlMessage
//                       });
//                   }else{
                      
//                       if(results != ''){
//                           callback(null, results);
//                           // res.send({
//                           //     "code": 200,
//                           //     "response":results,
//                           //     "status": 0,
//                           //     "message": 'Login SuccessFully'
//                           // });
//                       }else{
//                           res.send({
//                               "code": 200,
//                               "response": {},
//                               "status": 0,
//                               "message": 'Error In get Brand Id '
//                           });
//                        }
//                   }
//               });
//           },
//           function(results, callback) {
//           let productDescription=   item.productBaseInfoV1.productDescription;
//           if(productDescription=="")
//           {
//               productDescription=null
//           // }
//           }else{
//                var  p1=productDescription.replace(/['"]+/g, '')
//              productDescription= p1;
                
//           }
//               /* To get master user id */
//                  var product_details='INSERT INTO  ' + constant.product_master+ '(`brand_id`, `product_name`,`product_description`,`product_price_flipkart`,`product_type`,`product_id`) VALUES ('+results[0].id+',"'+item.productBaseInfoV1.title+'","'+productDescription+'",'+item.productBaseInfoV1.flipkartSellingPrice.amount +',"MOBILE","'+item.productBaseInfoV1.productId+'")';
//                   // console.log(product_details)
//                  database.getConn(product_details, function (err, detail_results) {
//                   if(err){
//                       console.log("---------------------------1-----------------------",constant.general_error)
                      
//                       res.send({
//                           "code": 200,
//                           "response": {},
//                           "status": 0,
//                           "message": err.sqlMessage
//                       });
//                   }else{
//                       if(detail_results != ''){
                        
//                           callback(null, results,detail_results);
//                       }else{
//                           res.send({
//                               "code": 200,
//                               "response": {},
//                               "status": 0,
//                               "message": constant.general_error
//                           });
//                       }
//                   }
//               });
//           },
//           function(results,detail_results,callback) {
//                   var p=item.productBaseInfoV1.imageUrls
//                   for(var key in p) {

//                   if (p.hasOwnProperty(key)) {
//                       // console.log(key)
                
//                              var image_details="INSERT INTO  " + constant.product_image+ "( `product_id`,`image_size`, `image_url`, `status`) VALUES ("+detail_results.insertId+",'"+key+"','"+p[key]+"','1')";
//                               // console.log(image_details)
//                              database.getConn(image_details, function (err, detail_results) {
//                             if(err){
//                               console.log("--------------2--655655656565656----------------------------------",err.sqlMessage)
                              
//                                 res.send({
//                                     "code": 200,
//                                     "response": {},
//                                     "status": 0,
//                                     "message": err.sqlMessage
//                                 });
//                             }else{
//                                 if(detail_results != ''){
                                  
//                                   //   callback(null, results,detail_results);
//                                 }else{
//                                   //   console.log("--------------------------------------------------",constant.general_error)
//                                     res.send({
//                                         "code": 200,
//                                         "response": {},
//                                         "status": 0,
//                                         "message": constant.general_error
//                                     });
//                                 }
//                             }
//                         });
//                   }
//               }
//               callback(null,detail_results)
//            },
//            function(detail_results,callback)
//            {
//               //  console.log(item)
//                forEach(item.categorySpecificInfoV1.specificationList, function(item2, index2, arr) {
//                   forEach(item2.values, function(item3, index3, arr) {
//                       // console.log("----------------------------------",item3)
//                       var p=item3;
//                       // for(var key in p) {

//                       //  console.log(key + " -> " +JSON.stringify(p) ); 
                      

//                        var P_specification='INSERT INTO ' + constant.mobile_specifications+ '(`product_id`, `specification`, `specification_description`) VALUES ('+detail_results.insertId+',"'+item3.key+'","'+item3.value+'")';
//                       //  console.log(P_specification)
//                       database.getConn(P_specification, function (err, detail_results) {
//                      if(err){
//                       console.log("=====3==========",P_specification)
//                       console.log("--999999999999999999999999999999999999999999999999999999999999999999999999999999999999999-----",constant.general_error)
                      
//                          res.send({
                             
//                              "code": 200,
//                              "response": {},
//                              "status": 0,
//                              "message": err.sqlMessage
//                          });
//                      }else{
//                          if(detail_results != ''){
                           
//                            //   callback(null, results,detail_results);
//                          }else{
//                               res.send({
//                                  "code": 200,
//                                  "response": {},
//                                  "status": 0,
//                                  "message": constant.general_error
//                              });
//                          }
//                      }
//                  });
//               // }
//           })
//         })
     
                      

//            }

//        ]
     
//   );
//   })

//  res.send({
//                               "code": 200,
//                               "response":"" ,
//                               "status": 1,
//                               "message": "Add All Mobile Data Successfully have successfully logged in"
//                           });
  
// }
