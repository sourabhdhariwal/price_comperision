'use strict';

/*
 * Purpose : For Admin Basic Api
 * Company : Mobiweb Technology Pvt. Ltd.
 * Package : User
 */

var Admin     = exports;
var async    = require('async');
var appRoot  = require('app-root-path');
var constant = require(appRoot + '/config/constant.js');
var custom   = require(appRoot + '/lib/custom.js');
var database = require(appRoot + '/config/database.js');
var model    = require(appRoot + '/lib/model.js');
var randomString = require('random-string');

Admin.adminLogin = function(req, res){

  /* Manage all validations */
  req.sanitize("userEmail").trim();
  req.sanitize("userPassword").trim();
  req.check('userEmail', 'Enter your email').notEmpty();
  req.check('userEmail', 'Enter a valid email').isEmail();
  req.check('userPassword', 'Enter password').notEmpty();
 

  let errors = req.validationErrors();
  if (errors) {
      res.send({
          "code": 200,
          "response": {},
          "status": 0,
          "message": custom.manageValidationMessages(errors)
      });
  }

   else{

    let userEmail    = req.sanitize('userEmail').escape().trim();
    let userPassword = custom.getMd5Value(req.sanitize('userPassword').escape().trim());

    var user_query = 'SELECT * FROM ' + constant.user_details + ' WHERE userEmail="'+userEmail+'"  AND userPassword = "'+userPassword+'" AND userType = "ADMIN"';
    
      database.getConn(user_query, function (err, results) {
       
          if(err){
             
              res.send({
                  "code": 200,
                  "response": {},
                  "status": 0,
                  "message": err.sqlMessage
              });
          }else{
              
              if(results != ''){
                  res.send({
                      "code": 200,
                      "response":results,
                      "status": 1,
                      "message": 'Login SuccessFully'
                  });
              }else{
                  res.send({
                      "code": 200,
                      "response": {},
                      "status": 0,
                      "message": 'Login credentials are not correct'
                  });
               }
          }
      });

   }

}

Admin.forgotpassword = function(req, res){

    req.sanitize("userEmail").trim();
    req.sanitize("newPassword").trim();
    req.sanitize("userEmail").trim();
    req.check('userEmail', 'The User email is required').notEmpty();
    req.check('newPassword', 'Enter new password').notEmpty();
    req.check('newPassword', 'The New Password field must contain at least 6 characters, including UPPER/lower case & numbers & at-least a special character').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/, "i");;
    req.check('confirmPassword', 'Confirm your password').notEmpty();
    req.check('confirmPassword', 'The Confirm Password field must contain at least 6 characters, including UPPER/lower case & numbers & at-least a special character').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/, "i");;
    req.check('confirmPassword', 'Your passwords did not match').equals(req.body.newPassword);
    let errors = req.validationErrors();
    if (errors) {
        res.send({
            "code": 200,
            "response": {},
            "status": 0,
            "message": custom.manageValidationMessages(errors)
        });
    } else {
        let userEmail           = req.sanitize('userEmail').escape().trim();
        let newPassword         = req.sanitize('newPassword').escape().trim();
        let confirmPassword     = req.sanitize('confirmPassword').escape().trim();

        var user_query = 'SELECT * FROM ' + constant.user_details + ' WHERE userEmail="'+userEmail+'"  AND userType = "ADMIN"';
        database.getConn(user_query, function (err, results) {
             
             if(err){
                  
                   res.send({
                       "code": 200,
                       "response": {},
                       "status": 0,
                       "message": err.sqlMessage
                   });
               }else{
                   
                   if(results != ''){
                       
                 /* Update user details */
                        var dataObj = {};
                        dataObj.userPassword = custom.getMd5Value(newPassword);
                        model.updateData(function(err,update_resp){
                            if(err){
                                res.send(custom.dbErrorResponse());
                                return false;
                            }else{
                                res.send({
                                    "code": 200,
                                    "response": {},
                                    "status": 1,
                                    "message": 'Your password has been changed successfully.'
                                });return false;
                            }
                        },constant.user_details,dataObj,{userEmail:userEmail});
 

                   }else{
                       res.send({
                           "code": 200,
                           "response": {},
                           "status": 0,
                           "message": 'Login credentials are not correct'
                       });
                    }


               }

           });

           
        
    } 

}

Admin.getUserdetails = function(req, res){

    var user_query = 'SELECT * FROM ' + constant.user_details + ' WHERE userType = "USER" ';
    database.getConn(user_query, function (err, results) {
      //console.log(results);return false;
    if(err){
        res.send(custom.dbErrorResponse());
        return false;
   
    }

    else{
        let user_response = results;
        res.send({
            "code": 200,
            "response": user_response,
            "status": 1,
            "message": "success"
        });
    }
    });

}

Admin.deleteUser = function(req, res){

  //  req.sanitize("id").trim();
  //  req.check('id', 'The User id is required').notEmpty();
    
  req.check('id', 'The User id is required').notEmpty();


    var delete_user = 'DELETE FROM  '+ constant.user_details + ' WHERE id = '+req.body.id+' ';
    database.getConn(delete_user, function (err, results) {    

        if(err){
            res.send(custom.dbErrorResponse());
            return false;
        }  

        else{
            
            res.send({
                "code": 200,
                "response": {},
                "status": 1,
                "message": "successfully deleted"
            });
        }

    });
}

Admin.blockUser_unblockuser= function(req, res){
    let status;
    var message;    
    if(req.body.type=="Block")
        {
         status =1 ;
         message="User successfully blocked";

        }
        else{
           status=0; 
           message="User successfully Unblocked"
        }
        req.check('id', 'The User id is required').notEmpty();
        
        let id = req.check('id', 'The User id is required').notEmpty();

        let user_detail = 'UPDATE ' + constant.user_details + ' SET isUserBlocked = '+status+'  WHERE id ='+req.body.id+'';
        database.getConn(user_detail, function (err, results) {    
    
            if(err){
                res.send(custom.dbErrorResponse());
                return false;
            }  
    
            else{
                
                res.send({
                    "code": 200,
                    "response": {},
                    "status": 1,
                    "message": message
                });
            }
    
        });
    }



