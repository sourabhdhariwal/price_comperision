'use strict';

/*
 * Purpose : For Admin Basic Api
 * Company : Mobiweb Technology Pvt. Ltd.
 * Package : User
 */

var Brand     = exports;
var async    = require('async');
var appRoot  = require('app-root-path');
var constant = require(appRoot + '/config/constant.js');
var custom   = require(appRoot + '/lib/custom.js');
var database = require(appRoot + '/config/database.js');
var model    = require(appRoot + '/lib/model.js');
var randomString = require('random-string');


 

  Brand.getAllBrands = function(req, res){
    console.log("in Brands")
    var user_query = 'SELECT * FROM ' + constant.brand_table + '';
    database.getConn(user_query, function (err, results) {
      //console.log(results);return false;
    if(err){
        res.send(custom.dbErrorResponse());
        return false;
   
    }

    else{
        let brand_data = results;
        res.send({
            "code": 200,
            "response": brand_data,
            "status": 1,
            "message": "success"
        });
    }
    });

}



// Brand.DeleteBrands = function(req, res){
//     console.log("in Brands")
//     var user_query = 'DELETE \ FROM ' + constant.brand_table + 'WHERE id ='+req.body.id+'';
//     database.getConn(user_query, function (err, results) {
//       //console.log(results);return false;
//     if(err){
//         res.send(custom.dbErrorResponse());
//         return false;
   
//     }

//     else{
  
//         res.send({
//             "code": 200,
//             "response": {},
//             "status": 1,
//             "message": "Successfully Delete Brand"
//         });
//     }
//     });

// }



