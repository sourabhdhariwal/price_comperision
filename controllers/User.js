'use strict';

/*
 * Purpose : For User Basic Api
 * Company : Mobiweb Technology Pvt. Ltd.
 * Developed By  : Sorav Garg
 * Package : User
 */

var User     = exports;
var async    = require('async');
var appRoot  = require('app-root-path');
var constant = require(appRoot + '/config/constant.js');
var custom   = require(appRoot + '/lib/custom.js');
var database = require(appRoot + '/config/database.js');
var model    = require(appRoot + '/lib/model.js');
var randomString = require('random-string');

User.userSignup = function(req, res) {
    
        /* Manage all validations */
        req.sanitize("userName").trim();
        req.sanitize("userEmail").trim();
        req.sanitize("userPassword").trim();
      
        req.check('userName','Enter your name').notEmpty();
        req.check('userEmail', 'Enter your email').notEmpty();
        req.check('userEmail', 'Enter a valid email').isEmail();
        req.check('userPassword', 'Enter password').notEmpty();
        req.check('userPassword', 'The Password field must contain at least 6 characters, including UPPER/lower case & numbers & at-least a special character').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/, "i");;
        req.check('userPhoneno','Enter your mobile number').notEmpty();
        req.check('userPhoneno','Enter valid mobile number').isNumeric();
        req.check('userPhoneno','Mobile number length should be 10 digit').validatePhoneNo();
       

       var OTP= Math.floor(100000 + Math.random() * 900000);
       var errors = req.validationErrors();
        if (errors) {
          
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "params":errors[0].param,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
          
            /* To get user phone number */
            let userPhoneno   = req.sanitize('userPhoneno').escape().trim();
          
              /* Check Unique phone number */
                database.getConn('SELECT * FROM ' + constant.user_details + ' WHERE userPhoneno = "' + userPhoneno + '"', function(err, rows){
                    if (err) {
                        res.send({"code": 200,"response": {},"status": 0,"message": err.sqlMessage});
                        return false;
                        } else {
                   
                            if (rows != '') {
                                 res.send({"code": 200,"response": {},"status": 0,"message": 'Mobile is already taken'});
                                return false;
                            }
                            else {
                                 let userEmail   = req.sanitize('userEmail').escape().trim();
                            
                                /* Check Unique phone number */
                                database.getConn('SELECT * FROM ' + constant.user_details + ' WHERE userEmail = "' + userEmail + '"', function(err, rows){
                                     if (err) {
                                        res.send({"code": 200,"response": {},"status": 0,"message": err.sqlMessage});
                                        return false;
                                      } else {
                                        if (rows != '') {
                                            res.send({"code": 200,"response": {},"status": 0,"message": 'Email is already taken'});
                                            return false;
                                        }
                                          else {


                                            /* Filter User Requested Data */
                                            let user_data = {};
                                            user_data.signup_token = signup_token;
                                            user_data.userName = req.sanitize('userName').escape().trim();
                                            user_data.userEmail = req.sanitize('userEmail').escape().trim();
                                            user_data.userPassword = custom.getMd5Value(req.sanitize('userPassword').escape().trim());
                                            user_data.userPhoneno = userPhoneno;
                                            

                            
                                             /* Insert user details data */
                                                   let user_details_query1 = 'INSERT INTO ' + constant.temp_details + ' (signup_token,userName, userEmail, userPassword, userPhoneno, OTP) VALUES ("'+signup_token+'","' + user_data.userName + '","' + user_data.userEmail + '","' + user_data.userPassword + '","' + user_data.userPhoneno + '","' + OTP + '")';
                                                     database.getConn(user_details_query1, function(err, result) {
                                                          if (err) {
                                                             
                                                            
                                                            res.send({
                                                                "code": 200,
                                                                "response": {},
                                                                "status": 0,
                                                                "message": err.sqlMessage
                                                            });
                                                            return false;
                                                        
                                                    }
                                                    else{
                                                            let otp = custom.sentOTPsms(userPhoneno,OTP);
                                                        //console.log(result);
                                                        let user_detail = custom.getTempUserProfileResponse(user_data);
                                                        res.send({
                                                            "code": 200,
                                                            "response": user_detail,
                                                            "status": 1,
                                                            "message": 'Success'
                                                        });
                                                        return false;
                                                      }

                                                });
                             }
                           }
                       });
                     }
                  }
            });
        }
    }
  
    
    
    
    

/**
 * To verify OTP
 * @param {string} signup_token
 * @param {string} OTP
 */

User.verifyOTP = function(req, res) {
   
    // req.check('signup_token', 'signup token is required').notEmpty();
    req.check('OTP', 'Enter your OTP').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
    } else{
     
        let signup_token   = req.sanitize('signup_token').trim();
        let OTP = req.sanitize('OTP').trim();
     
        var user_query = 'SELECT * FROM ' + constant.temp_details + ' WHERE  OTP = "'+OTP+'"';
       console.log(user_query);
         database.getConn(user_query, function (err, results) {
            console.log(results);
           
             if(err){
                
                 res.send({
                     "code": 200,
                     "response": {},
                     "status": 0,
                     "message": err.sqlMessage
                 });
             }else{
                 
                 if(results != ''){
              
                     var name = results[0].userName;
                     var email = results[0].userEmail;
                     var password = results[0].userPassword;
                     var phoneno = results[0].userPhoneno;
                     var userLoginSessionKey = custom.getGuid();
                     // var userType = 'USER';
                     let user_details = 'INSERT INTO ' + constant.user_details + ' (userName, userEmail, userPassword, userPhoneno, userLoginSessionKey) VALUES ("' + name + '","' + email + '","' + password + '","' + phoneno + '","' + userLoginSessionKey + '")';
                    console.log(user_details);

                    database.getConn(user_details, function(err, result) {
                        if (err) {
                                return res.send({
                                    "code": 200,
                                    "response": {},
                                    "status": 0,
                                    "message": 'failed'
                                });
                        }
                        else{
                            if(parseInt(result.insertId) > 0){
                                return res.send({
                                        "code": 200,
                                        "response":{},
                                        "status": 1,
                                        "message": 'sign-up SuccessFully'
                                    });
                            }else{
                                return res.send({
                                            "code": 200,
                                            "response": {},
                                            "status": 0,
                                            "message": 'failed'
                                        });
                            }
                        }
                    });

                 }else{
                     res.send({
                         "code": 200,
                         "response": {},
                         "status": 0,
                         "message": 'OTP  not verified'
                     });
                  }
             }
         });
        }
}

/**
 * To resend otp
 * @param {string} 
 * @param {string} 
 */
User.resendOTP = function(req, res) {

    req.check('signup_token', 'signup token is required').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        res.send({
            "code": 200,
            "response": {},
            "status": 0,
            "message": custom.manageValidationMessages(errors)
        });
    } else{
        
                let signup_token   = req.sanitize('signup_token').trim();
             
             
                var user_query = 'SELECT * FROM ' + constant.temp_details + ' WHERE signup_token="'+signup_token+'"';
               // console.log(user_query);
                 database.getConn(user_query, function (err, results) {
                   // console.log('err',err);
                    //console.log('results',results);
                    var userPhoneno = results[0].userPhoneno;
                     //console.log(userPhoneno);
                     if(err){
                        
                         res.send({
                             "code": 200,
                             "response": {},
                             "status": 0,
                             "message": err.sqlMessage
                         });
                     }else{
                         
                         if(results != ''){
                             
                            var newOTP= Math.floor(100000 + Math.random() * 900000);
                            
                         let new_otp = custom.sentOTPsms(userPhoneno,newOTP);

                            let user_detail = "UPDATE " + constant.temp_details + " SET OTP = '"+newOTP+"'  WHERE signup_token ="+"'"+signup_token+"'";
                          // console.log(user_detail);return false;
                            database.getConn(user_detail, function (err, detail_results) {
                                
                                if(err){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": err.sqlMessage
                                    });
                                }else{
                                    
                                    res.send({
                                        "code": 200,
                                        "response":{},
                                        "status": 1,
                                        "message": 'OTP send SuccessFully'
                                    });
                                }
                            });
                        
                         }else{
                             res.send({
                                 "code": 200,
                                 "response": {},
                                 "status": 0,
                                 "message": 'sign-up token not verified'
                             });
                          }
                     }
                 });
                }
            }

/**
 * To manage user login
 * @param {string} userEmail
 * @param {string} userPassword
 */

User.userLogin = function(req, res) {
    
        /* Manage all validations */
        req.sanitize("userEmail").trim();
        req.sanitize("userPassword").trim();
        req.check('userEmail', 'Enter your email').notEmpty();
        req.check('userEmail', 'Enter a valid email').isEmail();
        req.check('userPassword', 'Enter password').notEmpty();
        
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            var randString = randomString({length: 20});
            var timestamp =(new Date).getTime();
            var signup_token = timestamp+'-'+randString.substr(0, 8) + '-' +  randString.substr(8, 4) + '-' + 
                randString.substr(12, 4) + '-' + randString.substr(16, 4) + '-' + randString.substr(20);
            let userEmail    = req.sanitize('userEmail').escape().trim();
            let userPassword = custom.getMd5Value(req.sanitize('userPassword').escape().trim());
    
            /* Check User Email Id and Password */
            async.waterfall([
                function(callback) {
                    var user_query = 'SELECT * FROM ' + constant.user_details + ' WHERE userEmail="'+userEmail+'"  AND userPassword = "'+userPassword+'"';
                  // console.log(user_query);
                    database.getConn(user_query, function (err, results) {
                       // console.log(results);
                      
                        if(err){
                           
                            res.send({
                                "code": 200,
                                "response": {},
                                "status": 0,
                                "message": err.sqlMessage
                            });
                        }else{
                            
                            if(results != ''){
                                    var sess = req.session;
                                    sess.email=req.body.userEmail;
                                    req.session.save();
                                    console.log(req.session.email)
                                    callback(null, results);
                            }else{
                                res.send({
                                    "code": 200,
                                    "response": {},
                                    "status": 0,
                                    "message": 'Login credentials are not correct   '
                                });
                             }
                        }
                    });
                },
                function(results, callback) {
                   
                    /* To get master user id */
                  
                    var user_details_query = 'SELECT id FROM ' + constant.user_details+ ' WHERE userEmail="'+userEmail+'"';

                    database.getConn(user_details_query, function (err, detail_results) {
                        
                        if(err){
                            res.send({
                                "code": 200,
                                "response": {},
                                "status": 0,
                                "message": err.sqlMessage
                            });
                        }else{
                            if(detail_results != ''){

                                callback(null, results,detail_results);
                            }else{
                                res.send({
                                    "code": 200,
                                    "response": {},
                                    "status": 0,
                                    "message": constant.general_error
                                });
                            }
                        }
                    });
                },
                function(results,detail_results,callback) {
                    // console.log("hii  dsgdsg")
                    // console.log(detail_results)
                    if(parseInt(detail_results[0].userEmailVerified) === 0){
                        res.send({
                                "code": 200,
                                "response": {"userLoginSessionKey":detail_results[0].userLoginSessionKey},
                                "status": 5,
                                "message": constant.email_verify
                            });
                        return false;
                    }else if(parseInt(detail_results[0].isUserBlocked) === 1){
                        res.send({
                                "code": 405,
                                "response": {},
                                "status": 0,
                                "message": constant.user_blocked
                            });
                        return false;
                    }else if(parseInt(detail_results[0].isUserDeactivated) === 1){
                        res.send({
                                "code": 405,
                                "response": {},
                                "status": 0,
                                "message": constant.user_deactivated
                            });
                        return false;
                    }else{
                        callback(null, results,detail_results);
                    }
                }
            ], function (err, results,detail_results) {

             let user_details_query = "UPDATE " + constant.user_details + " SET userLastLogin = '"+custom.getCurrentTime()+"' , userLastIpAddress = '"+custom.getUserIp()+ "' WHERE id ="+detail_results[0].id;
                   // console.log(user_details_query);
                    database.getConn(user_details_query, function (err, detail_results) {
                        
                        if(err){
                            res.send({
                                "code": 200,
                                "response": {},
                                "status": 0,
                                "message": err.sqlMessage
                            });
                        }else{
                            let user_response = custom.getUserProfileResponse(results);
                            res.send({
                                "code": 200,
                                "response": user_response,
                                "status": 1,
                                "message": "You have successfully logged in"
                            });
                        }
                    });
                
            });
        }
    }
 
 /**
 * To manage user logout
 * @param {string} 
 * @param {string} 
 */   
User.logout =function(req , res){
   
  
        req.session.destroy(function(err) {
          if(err) {
            console.log(err);
          } else {
            // res.redirect('/');
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": 'User logout successfully'
            }); 
          }
        });

}

/**
 * To forgot password request
 * @param {string} userEmail
*/

User.forgotPassword = function(req, res) {
    
        req.sanitize("userEmail").trim();
        req.check('userEmail', 'Enter your email').notEmpty();
        req.check('userEmail', 'Enter a valid email').isEmail();
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            let userEmail = req.sanitize('userEmail').escape().trim();
            
            async.waterfall([
                function(callback) {
                    /* Get user email id */
                    model.getAllWhere(function(err,resp){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(resp != ""){
                                callback(null,resp,resp[0].id);
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : 'Your email is not correct'});
                                return false;
                            }
                        }
                    },constant.user_details,{userEmail:userEmail});              
                },
                function(results, id, callback) {
                 // console.log("hii");
                    /* Get user details */
                    model.getAllWhere(function(err,resp){
                      //  console.log(results);return false;
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(resp != ""){
                              
                                if(resp[0].userEmailVerified == 0){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.email_verify
                                    }); 
                                    return false;
                                }else if(resp[0].isUserBlocked == 1){
                                    res.send({
                                        "code": 405,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.user_blocked
                                    }); 
                                    return false;
                                }else if(resp[0].isUserDeactivated == 1){
                                    res.send({
                                        "code": 405,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.user_deactivated
                                    }); 
                                    return false;
                                }else if(parseInt(resp[0].isSocialSignup) === 1){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": 'Social users can`t forgot password'
                                    }); 
                                    return false;
                                }else{
                                    let getTempCode = custom.generateRandomNo(6);    
                                   
                                    /* Update user details */
                                    var dataObj = {};
                                    dataObj.userTempCode = getTempCode;
                                    dataObj.userTempCodeSentTime = custom.getCurrentTime();
                                   
                                    model.updateData(function(err,update_resp){
                                        if(err){
                                            
                                            res.send(custom.dbErrorResponse());
                                            return false;
                                        }else{
                                           
                                            callback(null,resp[0].userLoginSessionKey,resp[0].userName,results[0].userEmail,results[0].id,getTempCode);
                                        }
                                    },constant.user_details,dataObj,{id:resp[0].id});
                    
                                }
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.user_details_not_found});
                                return false;
                            }
                        }
                    },constant.user_details,{id:id});
                }
            ], function (err,userLoginSessionKey,userName,userEmail,id,getTempCode) {
                
                /* Send temporary code on user email id */
                let siteName = constant.site_name;
                let forgotPasswordMsg = custom.forgotPasswordMsg(userName,getTempCode);
                let mailData = {};
                mailData.to_email = userEmail;
                mailData.subject  = constant.forgot_password_subject;
                mailData.message  = forgotPasswordMsg;
                custom.sendEmailCallBack(mailData,function(err,resp){
                    if(err){
                        res.send(custom.mailErrorResponse());
                        return false;
                    }else{
                        res.send({
                            "code": 200,
                            "response": {userLoginSessionKey:userLoginSessionKey},
                            "status": 1,
                            "message": constant.forgot_pswd_msg
                        });
                        return false; 
                    }
                });
            });
        }
    }
    
 /**
 * To verify user forgot password code
 * @param {string} userTempCode
 * @param {string} userLoginSessionKey
*/

User.verifyForgotPasswordCode = function(req, res) {
    
        req.sanitize("userTempCode").trim();
        req.sanitize("userLoginSessionKey").trim();
        req.check('userTempCode', 'Enter your temporary code').notEmpty();
        req.check('userLoginSessionKey', 'The User login session key field is required').notEmpty();
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            let userTempCode        = req.sanitize('userTempCode').escape().trim();
            let userLoginSessionKey = req.sanitize('userLoginSessionKey').escape().trim();
    
            async.waterfall([
                function(callback) {
    
                   /* Get user details */
                    model.getAllWhere(function(err,results){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(results != ""){
                                model.getAllWhere(function(err,code_results){
                                    if(err){
                                        res.send(custom.dbErrorResponse());
                                        return false;
                                    }else{
                                        if(code_results != ""){
                                            if(results[0].id == code_results[0].id){
                                                if(code_results[0].userEmailVerified == 0){
                                                    res.send({
                                                        "code": 200,
                                                        "response": {},
                                                        "status": 0,
                                                        "message": constant.email_verify
                                                    }); 
                                                    return false;
                                                }else if(code_results[0].isUserBlocked == 1){
                                                    res.send({
                                                        "code": 405,
                                                        "response": {},
                                                        "status": 0,
                                                        "message": constant.user_blocked
                                                    }); 
                                                    return false;
                                                }else{
                                                    let userTempCodeSentTime  = code_results[0].userTempCodeSentTime;
                                                    let currentTime  = custom.getCurrentTime();
                                                    let timeDifferece = custom.getDateTimeDifference(userTempCodeSentTime,currentTime,'minutes');
                                                    if(timeDifferece > constant.code_valid_time){
                                                        res.send({
                                                            "code": 200,
                                                            "response": {userLoginSessionKey:userLoginSessionKey},
                                                            "status": 0,
                                                            "message": constant.code_limit_msg
                                                        });
                                                    }else{
                                                        callback(null,results,results[0].id);
                                                    }
                                                }
                                            }else{
                                                res.send({
                                                    "code": 200,
                                                    "response": {},
                                                    "status": 0,
                                                    "message": constant.invalid_code
                                                });
                                            }
                                        }else{
                                            res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.invalid_forgot_code});
                                            return false;
                                        }
                                    }
                                },constant.user_details,{userTempCode:userTempCode});
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.invalid_login_session_key});
                                return false;
                            }
                        }
                    },constant.user_details,{userLoginSessionKey:userLoginSessionKey}); 
                }
            ], function (err, results,id) {
                
                /* Update user details */
                var dataObj = {};
                dataObj.userTempCode = null;
                dataObj.userTempCodeSentTime = null;
                model.updateData(function(err,update_resp){
                    if(err){
                        res.send(custom.dbErrorResponse());
                        return false;
                    }else{
                        res.send({
                            "code": 200,
                            "response": {userLoginSessionKey:results[0].userLoginSessionKey},
                            "status": 1,
                            "message": 'Your temporary code successfully verified with us, please reset your password.'
                        });
                    }
                },constant.user_details,dataObj,{id:id});
    
            });
        }
    } 
    
   /**
 * To reset user password
 * @param {string} userLoginSessionKey
 * @param {string} newPassword
 * @param {string} confirmPassword
*/

User.resetPassword = function(req, res) {
    
        req.sanitize("userLoginSessionKey").trim();
        req.sanitize("newPassword").trim();
        req.sanitize("confirmPassword").trim();
        req.check('userLoginSessionKey', 'The User login session key field is required').notEmpty();
        req.check('newPassword', 'Enter new password').notEmpty();
        req.check('newPassword', 'The New Password field must contain at least 6 characters, including UPPER/lower case & numbers & at-least a special character').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/, "i");;
        req.check('confirmPassword', 'Confirm your password').notEmpty();
        req.check('confirmPassword', 'The Confirm Password field must contain at least 6 characters, including UPPER/lower case & numbers & at-least a special character').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/, "i");;
        req.check('confirmPassword', 'Your passwords did not match').equals(req.body.newPassword);
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            let userLoginSessionKey = req.sanitize('userLoginSessionKey').escape().trim();
            let newPassword         = req.sanitize('newPassword').escape().trim();
            let confirmPassword     = req.sanitize('confirmPassword').escape().trim();
    
            async.waterfall([
                function(callback) {
                   
                    /* Get user details */
                    model.getAllWhere(function(err,results){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(results != ""){
                                if(results[0].userEmailVerified == 0){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.email_verify
                                    }); 
                                    return false;
                                }else if(results[0].isUserBlocked == 1){
                                    res.send({
                                        "code": 405,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.user_blocked
                                    }); 
                                    return false;
                                }else{
                                    callback(null,results,results[0].id);
                                }
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.invalid_login_session_key});
                                return false;
                            }
                        }
                    },constant.user_details,{userLoginSessionKey:userLoginSessionKey}); 
                }
            ], function (err,results,id) {
    
                /* Update user details */
                var dataObj = {};
                dataObj.userPassword = custom.getMd5Value(newPassword);
                model.updateData(function(err,update_resp){
                    if(err){
                        res.send(custom.dbErrorResponse());
                        return false;
                    }else{
                        res.send({
                            "code": 200,
                            "response": {},
                            "status": 1,
                            "message": 'Your password has been changed successfully.'
                        });
                    }
                },constant.user_details,dataObj,{id:id});
            });
        }
    }
   
    
/**
 * To re-send forgot password code
 * @param {string} userLoginSessionKey
*/

User.resendForgotPasswordCode = function(req, res) {
    
        req.sanitize("userLoginSessionKey").trim();
        req.check('userLoginSessionKey', 'The User login session key field is required').notEmpty();
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            let userLoginSessionKey = req.sanitize('userLoginSessionKey').escape().trim();
            
            async.waterfall([
                function(callback) {
    
                    /* Get user details */
                    model.getAllWhere(function(err,resp){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(resp != ""){
                                if(resp[0].userEmailVerified == 0){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.email_verify
                                    }); 
                                    return false;
                                }else if(resp[0].isUserBlocked == 1){
                                    res.send({
                                        "code": 405,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.user_blocked
                                    }); 
                                    return false;
                                }else{
                                    let userLastTempCodeSentTime  = resp[0].userTempCodeSentTime;
                                    let currentTime  = custom.getCurrentTime();
                                    let timeDifferece = custom.getDateTimeDifference(userLastTempCodeSentTime,currentTime,'minutes');
                                    if(timeDifferece < constant.resend_code_limit){
                                        res.send({
                                            "code": 200,
                                            "response": {userLoginSessionKey:userLoginSessionKey},
                                            "status": 0,
                                            "message": 'Sorry !! you can make new request after '+constant.resend_code_limit+' minutes.'
                                        });
                                        return false;
                                    }else{
                                       /* Get user email id */
                                        model.getAllWhere(function(err,main_results){
                                            if(err){
                                                res.send(custom.dbErrorResponse());
                                                return false;
                                            }else{
                                                if(main_results != ""){
                                                    callback(null,resp,main_results[0].userEmail);
                                                }else{
                                                    res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.user_detais_not_found});
                                                    return false;
                                                }
                                            }
                                        },constant.user_details,{id:resp[0].id},'','','userEmail');
                                    }
                                }
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.invalid_login_session_key});
                                return false;
                            }
                        }
                    },constant.user_details,{userLoginSessionKey:userLoginSessionKey});
                },
                function(results,userEmail, callback) {
                    let getTempCode = constant.temp_code;    
    
                    /* Update user details */
                    var dataObj = {};
                    dataObj.userTempCode = getTempCode;
                    dataObj.userTempCodeSentTime = custom.getCurrentTime();
                    model.updateData(function(err,resp){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            callback(null,results,getTempCode,userEmail);
                        }
                    },constant.user_details,dataObj,{id:results[0].id});
                }
            ], function (err, results,getTempCode,userEmail) {
    
                /* Send verification email to user */
                let siteName = constant.site_name;
                let forgotPasswordMsg = custom.forgotPasswordMsg(results[0].userName,getTempCode);
                let mailData = {};
                mailData.to_email = userEmail;
                mailData.subject  = constant.forgot_password_subject;
                mailData.message  = forgotPasswordMsg;
                custom.sendEmailCallBack(mailData,function(err,resp){
                    if(err){
                        res.send(custom.mailErrorResponse());
                        return false;
                    }else{
                        res.send({
                            "code": 200,
                            "response": {userLoginSessionKey:userLoginSessionKey},
                            "status": 5,
                            "message": constant.forgot_pswd_msg
                        });
                        return false; 
                    }
                });
            });
        }
    }

   /**
 * To verify social login
 * @param {string} 
 * @param {string} 
*/ 
User.socialLogin = function(req, res) {
   
  req.check('userEmail', 'user email is required').notEmpty();
  req.check('social_type', 'social type is required').notEmpty();
  req.check('userName', 'user name is required').notEmpty();
  let errors = req.validationErrors();
    if (errors) {
        res.send({
            "code": 200,
            "response": {},
            "status": 0,
            "message": custom.manageValidationMessages(errors)
        });
    }

    else{

        let userEmail    = req.sanitize('userEmail').trim();
        let social_type  = req.sanitize('social_type').trim();
        let userName     = req.sanitize('userName').trim();

        let user_data ={};
        user_data.userEmail =  userEmail; 
        user_data.userName =  userName;
               

        var user_query = 'SELECT * FROM ' + constant.user_details + ' WHERE userEmail = "' + userEmail + '"';
 
              database.getConn(user_query, function (err, results) {
           
                
                  if(err){
                     
                      res.send({
                          "code": 200,
                          "response": {},
                          "status": 0,
                          "message": err.sqlMessage
                      });
                  }else{
                      
                      if(results != ''){
                          
                        let user_details_query = "UPDATE " + constant.user_details + " SET social_id = '"+userEmail+"', social_type = '"+social_type+"',userName = '"+userName+"' WHERE userEmail ="+"'"+userEmail+"'"
                          
                                database.getConn(user_details_query, function (err, detail_results) {
                                    
                                    if(err){
                                        res.send({
                                            "code": 200,
                                            "response": {},
                                            "status": 0,
                                            "message": err.sqlMessage
                                        });
                                    }else{
                                        let user_response = custom.getSocialUserProfileResponse(user_data);
                                        res.send({
                                            "code": 200,
                                            "response": user_response,
                                            "status": 1,
                                            "message": "successfully updated existing email id"
                                        });
                                    }
                                });
                         }  else{
                              
                         
           var user_query = 'SELECT * FROM ' + constant.user_details + ' WHERE social_id = "' + userEmail + '"';
        //   console.log(user_query);return false;
             database.getConn(user_query, function (err, results) {
               // console.log(results);return false;
                         if(err){
                            
                             res.send({
                                 "code": 200,
                                 "response": {},
                                 "status": 0,
                                 "message": err.sqlMessage
                             });
                         }
                            /* Insert user details data */
                      else{
                            if(results == ''){ 
                            
                                  
                            let user_detail = 'INSERT INTO ' + constant.user_details + ' (social_id,userName, social_type) VALUES ("' + userEmail + '","' + userName + '","' + social_type + '")';        
                            
                            database.getConn(user_detail, function(err, results) {
                               
                                if (err) {
                                    
                                        res.send({
                                            "code": 200,
                                            "response": {},
                                            "status": 0,
                                            "message": err.sqlMessage
                                        });
                                        return false;

                                }
                                else{
                                    
                                    let user_response = results;
                                    res.send({
                                        "code": 200,
                                        "response": user_response,
                                        "status": 1,
                                        "message": 'Successfully inserted new social id '
                                    });
                                    return false;
                                    
                                }
                            
                              });

                            }
                        else{

                             
                            let user_details_query = "UPDATE " + constant.user_details + " SET social_id = '"+userEmail+"', social_type = '"+social_type+"',userName = '"+userName+"' WHERE social_id ="+"'"+userEmail+"'"
                            
                                  database.getConn(user_details_query, function (err, detail_results) {
                                      
                                      if(err){
                                          res.send({
                                              "code": 200,
                                              "response": {},
                                              "status": 0,
                                              "message": err.sqlMessage
                                          });
                                      }else{
                                         // let user_response = custom.getSocialUserProfileResponse(user_data);
                                          res.send({
                                              "code": 200,
                                              "response": results,
                                              "status": 1,
                                              "message": "Successfully updated existing social id"
                                          });
                                      }
                                  });


                        }
                    }
                    });

                          }

                       
                        }
              });
         

    }


}


/**
 * To send email verification link 
 * @param {string} userEmail
*/

User.sendEmailVerificationLink = function(req, res) {
    
     var verifyToken =  randomString({length: 20});
       var verify = 'User/verifyEmail'
      var URL = constant.base_url + verify +'/'+ verifyToken;
       let verifyLink =  URL;  
    //  console.log(verifyLink);return false;
        req.sanitize("userEmail").trim();
        req.check('userEmail', 'Enter your email').notEmpty();
        req.check('userEmail', 'Enter a valid email').isEmail();
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            let userEmail = req.sanitize('userEmail').escape().trim();
               // console.log(userEmail); return false;
            async.waterfall([
                function(callback) {
                    /* Get user email id */
                    model.getAllWhere(function(err,resp){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(resp != ""){
                                callback(null,resp,resp[0].id);
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : 'Your email is not correct'});
                                return false;
                            }
                        }
                    },constant.user_details,{userEmail:userEmail});              
                },
                function(results, id, callback) {
                 
                    /* Get user details */
                    model.getAllWhere(function(err,resp){
                      //  console.log(results);return false;
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(resp != ""){
                              
                                if(resp[0].userEmailVerified == 0){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.email_verify
                                    }); 
                                    return false;
                                }else if(resp[0].isUserBlocked == 1){
                                    res.send({
                                        "code": 405,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.user_blocked
                                    }); 
                                    return false;
                                }else{
                                    
                                   
                                    /* Update user details */
                                    var dataObj = {};
                                    dataObj.userTempCode = verifyLink;
                                    dataObj.userTempCodeSentTime = custom.getCurrentTime();
                                   
                                    model.updateData(function(err,update_resp){
                                        if(err){
                                            
                                            res.send(custom.dbErrorResponse());
                                            return false;
                                        }else{
                                         
                                            callback(null,resp[0].userName,results[0].userEmail);
                                            //console.log(userEmail);return false;
                                        }
                                    },constant.user_details,dataObj,{id:resp[0].id});
                    
                                }
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.user_details_not_found});
                                return false;
                            }
                        }
                    },constant.user_details,{id:id});
                }
            ], function (err, userName, userEmail) {
          // console.log(userName);return false;
                /* Send verification email to user */
                let siteName = constant.site_name;
                let mailData = {};
                mailData.to_email = userEmail;
                mailData.subject  = constant.verification_subject;
                mailData.message  = custom.verificationMailMsg(userName,verifyLink);
                custom.sendEmailCallBack(mailData,function(err,resp){
                    if(err){
                        res.send(custom.mailErrorResponse());
                        return false;
                    }else{
                        res.send({
                            "code": 200,
                            "response": {},
                            "status": 5,
                            "message": "Account verification link sent successfully, Please check your registered mail to verify account."
                        });
                        return false; 
                    }
                });
            });
        }
    }
/**
 * To verify user email
 * @param {string} userTempCode
 * @param {string} userLoginSessionKey
*/

User.verifyEmail = function(req, res) {
        
        var tokens  = req.params.id;
    //  console.log(tokens);return false;
        var user_query = 'SELECT * FROM ' + constant.user_details+ ' WHERE userTempCode="'+tokens+'"';
        database.getConn(user_query, function (err, results) {
            if(err){
                res.send({
                    "code": 200,
                    "response": {},
                    "status": 0,
                    "message": err.sqlMessage
                });
            }else{
                if(results != ''){
                   
                    let user_details_query = "UPDATE " + constant.user_details + " SET 	userEmailVerified = '1' WHERE userEmail ="+"'"+userEmail+"'"
                    
                          database.getConn(user_details_query, function (err, detail_results) {
                              
                              if(err){
                                  res.send({
                                      "code": 200,
                                      "response": {},
                                      "status": 0,
                                      "message": err.sqlMessage
                                  });
                              }else{
                                  let user_response = custom.getUserProfileResponse(user_data);
                                  res.send({
                                      "code": 200,
                                      "response": user_response,
                                      "status": 1,
                                      "message": "Successfully updated"
                                  });
                              }
                          });

                }else{
                    res.send({
                        "code": 200,
                        "response": {},
                        "status": 0,
                        "message": 'invalid token'
                    });
                }
            }
        });

    }

   /**
 * To re-send account verification code
 * @param {string} userLoginSessionKey
*/

User.resendEmailVerificationCode = function(req, res) {
    
        req.sanitize("userLoginSessionKey").trim();
        req.check('userLoginSessionKey', 'The User login session key field is required').notEmpty();
        let errors = req.validationErrors();
        if (errors) {
            res.send({
                "code": 200,
                "response": {},
                "status": 0,
                "message": custom.manageValidationMessages(errors)
            });
        } else {
            let userLoginSessionKey = req.sanitize('userLoginSessionKey').escape().trim();
            
            async.waterfall([
                function(callback) {
    
                    /* Get user details */
                    model.getAllWhere(function(err,resp){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            if(resp != ""){
                                if(resp[0].userEmailVerified == 1){
                                    res.send({
                                        "code": 200,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.already_verified
                                    }); 
                                    return false;
                                }else if(resp[0].isUserBlocked == 1){
                                    res.send({
                                        "code": 405,
                                        "response": {},
                                        "status": 0,
                                        "message": constant.user_blocked
                                    }); 
                                    return false;
                                }else{
                                    let userLastTempCodeSentTime  = resp[0].userTempCodeSentTime;
                                    let currentTime  = custom.getCurrentTime();
                                    let timeDifferece = custom.getDateTimeDifference(userLastTempCodeSentTime,currentTime,'minutes');
                                    if(timeDifferece < constant.resend_code_limit){
                                        res.send({
                                            "code": 200,
                                            "response": {userLoginSessionKey:userLoginSessionKey},
                                            "status": 0,
                                            "message": 'Sorry !! you can make new request after '+constant.resend_code_limit+' minutes.'
                                        });
                                        return false;
                                    }else{
                                        /* Get user email id */
                                        model.getAllWhere(function(err,main_results){
                                            if(err){
                                                res.send(custom.dbErrorResponse());
                                                return false;
                                            }else{
                                                if(main_results != ""){
                                                    callback(null,resp,main_results[0].userEmail);
                                                }else{
                                                    res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.user_detais_not_found});
                                                    return false;
                                                }
                                            }
                                        },constant.users,{id:resp[0].id},'','','userEmail');
                                    } 
                                }
                            }else{
                                res.send({"code" : 200, "response" : {},"status" : 0,"message" : constant.invalid_login_session_key});
                                return false;
                            }
                        }
                    },constant.user_details,{userLoginSessionKey:userLoginSessionKey});
                },
                function(results,userEmail, callback) {
                    let getTempCode = constant.temp_code;    
    
                    /* Update user details */
                    var dataObj = {};
                    dataObj.userTempCode = getTempCode;
                    dataObj.userTempCodeSentTime = custom.getCurrentTime();
                    model.updateData(function(err,resp){
                        if(err){
                            res.send(custom.dbErrorResponse());
                            return false;
                        }else{
                            callback(null,results,getTempCode,userEmail);
                        }
                    },constant.user_details,dataObj,{id:results[0].id});
                }
            ], function (err, results,getTempCode,userEmail) {
    
                /* Send verification email to user */
                let siteName = constant.site_name;
                let mailData = {};
                mailData.to_email = userEmail;
                mailData.subject  = constant.verification_subject;
                mailData.message  = custom.verificationMailMsg(results[0].userName,getTempCode);
                custom.sendEmailCallBack(mailData,function(err,resp){
                    if(err){
                        res.send(custom.mailErrorResponse());
                        return false;
                    }else{
                        res.send({
                            "code": 200,
                            "response": {},
                            "status": 5,
                            "message": "Account verification code resent successfully, Please check your registered mail to verify account."
                        });
                        return false; 
                    }
                });
            });
        }
    } 

      