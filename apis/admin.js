var admin = require('../controllers/Admin.js');

module.exports = function(app, database, constant,custom){


 app.post('/Admin/adminLogin',function(req,res,next){
        admin.adminLogin(req,res);
        
    });


    app.post('/Admin/forgotpassword',function(req,res,next){
        admin.forgotpassword(req,res);
        
    });

    app.get('/Admin/getUserdetails',function(req,res,next){
        admin.getUserdetails(req,res);
        
    });

    app.post('/Admin/deleteUser',function(req,res,next){
        admin.deleteUser(req,res);
        
    });

    app.post('/Admin/blockUser',function(req,res,next){
        admin.blockUser_unblockuser(req,res);
        
    });
};