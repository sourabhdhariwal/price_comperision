
var user = require('../controllers/User.js');

module.exports = function(app, database, constant,custom){
   
    app.post('/User/userSignup',function(req,res,next){
        user.userSignup(req,res);
        
    });

    app.post('/User/verifyOTP',function(req,res,next){
        user.verifyOTP(req,res);
        
    });

    app.post('/User/resendOTP',function(req,res,next){
        user.resendOTP(req,res);
        
    });

    app.post('/User/userLogin',function(req,res,next){
        user.userLogin(req,res);
        
    });
    
    app.post('/User/forgotPassword',function(req,res,next){
        user.forgotPassword(req,res);
        
    });
    
    app.post('/User/verifyForgotPasswordCode',function(req,res,next){
        user.verifyForgotPasswordCode(req,res);
        
    });

    app.post('/User/resetPassword',function(req,res,next){
        user.resetPassword(req,res);
        
    });

    app.post('/User/resendForgotPasswordCode',function(req,res,next){
        user.resendForgotPasswordCode(req,res);
        
    });

    app.post('/User/socialLogin',function(req,res,next){
        user.socialLogin(req,res);
        
    });

    app.post('/User/logout',function(req,res,next){
        user.logout(req,res);
    });

    
    app.post('/User/sendEmailVerificationLink',function(req,res,next){
        user.sendEmailVerificationLink(req,res);
    });

    app.get('/User/verifyEmail/:id',function(req,res,next){
        user.verifyEmail(req,res);
    });


};