"use strict";

/*
 * Purpose: Running node.js and initialize. 
 * Authur : Sorav Garg
 * Company: Mobiweb Technology Pvt. Ltd.
*/
const app                = require('express')(),
	  express            = require('express'),
	  server             = require('http').Server(app),
	  database           = require('./config/database.js'),
	  bodyParser         = require('body-parser'),
	  upload             = require('multer')(),
	  dateTime           = require('date-time'),
	  expressValidator   = require('express-validator'),
	  custom             = require('./lib/custom.js'),
	  constant           = require('./config/constant.js'),
	  path               = require('path'),
	  session            = require('express-session');
	  
      
	  

/* For Session */
app.use(session({secret: 'ssshhhhh',saveUninitialized: true,resave: false, store: app.sessionStore,cookie:{path: "/"},name: "id"}));




/* For Validation */
app.use(expressValidator({
	customValidators: {

		/* For In List Validation */
		inList: function(value,allowed_values){
			if (allowed_values.indexOf(value) >= 0) {
			    return true;
			} else {
			    return false;
			}
		},

		/* To check minimum value */
		minValue: function(value,minValueLimit){
			if (parseInt(value) >= parseInt(minValueLimit)) {
			    return true;
			} else {
			    return false;
			}
		},

		/* To check phone no */
		validatePhoneNo: function(value){
			let isValid = /^\d{10}$/.test(value);
			console.log('isValid',isValid);
			if(isValid){
				return true;
			}else{
				return false;
			}
		}
	}
}));

// set the view engine to ejs
app.set('view engine', 'ejs');

/* To set port */
app.set('port', 4000);

/* For parsing urlencoded data */
app.use(bodyParser.urlencoded({ extended: true }));

/* For parsing application/json */
app.use(bodyParser.json());

/* To Listen Port */
server.listen(app.get('port'), function () {
  console.log(`Express server listening on port ${app.get('port')}`);
});

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.use('*', function(req, res,next) {
	// CORS headers
	var responseSettings = {
		"AccessControlAllowOrigin": req.headers.origin,
		"AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
		"AccessControlAllowMethods": "POST, GET, PUT, DELETE, OPTIONS",
		"AccessControlAllowCredentials": true
	};
		// Set custom headers for CORS
	res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
	res.header("Access-Control-Allow-Origin",  responseSettings.AccessControlAllowOrigin);
	res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : "x-requested-with");
	res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);
	if ('OPTIONS' == req.method) {
		res.send(200).end();
	}
	else {
		next();
	}
});





// Error Handler
// app.use(express.errorHandler());

require('./apis/user.js')(app, database, constant,custom);
require('./apis/admin.js')(app, database, constant,custom);
require('./apis/flipkart.js')(app, database, constant,custom);
require('./apis/brand.js')(app, database, constant,custom);
// module.exports = { app, io, socketUsers };

 /* to view code of angular*/
  app.use(express.static(path.join(__dirname, '/public')));
           app.get('/*',function(req,res){
           	    res.sendfile(path.join(__dirname, '/public/index.html'));
                
           });





//  app.listen(8080)



// console.log("Server Start")
/* End of file index.js */
/* Location: ./index.js */